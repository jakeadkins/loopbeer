let beerList;

async function fetchAPI() {
    let apiURL = "https://api.punkapi.com/v2/beers"
    beerList = await fetch(apiURL)
    .then ((response) => response.json())
    .then (function(data){
        return data;
    });
}

function renderBeerList(){
    let cardContainer = document.getElementById('card-container');
    beerList.forEach(beer => {
       cardContainer.append(renderBeerItem(beer))
    });
}

function  renderBeerItem(beer){

    let card = document.createElement('div');
    card.className = 'card, text-center, w-25 ';
    card.style = 'height:600px,';

    let img = document.createElement('img');
    img.className = 'img-thumbnail, mx-auto d-block';
    img.style= 'height:150px';
    img.src = beer.image_url;

    let cardbody = document.createElement('div');
    cardbody.className = 'card-body';
    cardbody.style= 'height:500px';

    let cardtitle = document.createElement('h5');
    cardtitle.className = 'card-header';
    cardtitle.innerText = beer.name;
    cardtitle.style = 'mx-auto d-block';

    let carddis = document.createElement('p');
    carddis.className = 'card-text';
    carddis.innerText = beer.description;

    let row = document.createElement('row');
    let button = document.createElement('a');
    button.className = 'btn btn-info';
    button.innerHTML = "More Info on " + beer.name;
    button.style = 'col text-center';

    button.addEventListener('click',() =>{
        console.log(beer.first_brewed);
        document.getElementById('modal-title').innerHTML= beer.name;
        document.getElementById('modal-body').innerHTML = 'Beers Tagline: '+ beer.tagline;
        document.getElementById('modal-body1').innerHTML = 'Brewers Tips: '+ beer.brewers_tips;
        document.getElementById('modal-body2').innerHTML = 'First Brewed: '+ beer.first_brewed;
        document.getElementById('modal-body3').innerHTML = 'Alcohol by volume: '+ beer.abv;

        $('#beerDetailsModal').modal('toggle');

    })

    cardbody.appendChild(cardtitle);
    card.appendChild(img);
    cardbody.appendChild(carddis);
    cardbody.appendChild(button);
    card.appendChild(cardbody);


    return card;

}

async function run (){
    await fetchAPI();
    renderBeerList();
}
run();